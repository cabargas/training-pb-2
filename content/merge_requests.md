## Merging (via CLI)


```
(on feature_branch) git push origin feature_branch
(on feature_branch) git checkout master
(on master) git merge feature_branch
(on master) git status
```

----------

## Merge requests

- Create your first merge request
  - Use the blue button in the activity feed
  - View the diff (changes) and leave a comment
  - Push a new commit to the same branch
  - Review the changes again and notice the update

