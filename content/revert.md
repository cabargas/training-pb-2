## Stash

Avoid commiting half-done work.

```
git stash
git stash apply
git stash drop
```

https://git-scm.com/book/en/v1/Git-Tools-Stashing

----------

## Prune

Prunes all unreachable objects

```
git prune
git prune -n (show to be deleted)
git prune -v (delete and show)
```

https://git-scm.com/docs/git-prune

----------

## Rebase

Apply commits on other head

```
git rebase
```

https://git-scm.com/docs/git-rebase

----------

## Tagging

Add tags to important versions

```
git tag
git tag -a v1.0 -m "my version 1.4"
```

https://git-scm.com/book/en/v2/Git-Basics-Tagging

----------

## Cherry-pick

Put specific commits on top of a different parent

```
git cherry-pick SHA-HASH
```
