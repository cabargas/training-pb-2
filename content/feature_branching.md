## Branching

1. Create a new branch called `squash_some_bugs`
2. Edit a file
3. Add your changes
3. Create a commit
4. Push
5. Commands

----------
```
git branch squash_some_bugs
git checkout squash_some_bugs
git status
# Edit `bugs.rb`
git status
git add bugs.rb
git commit -m 'Fixed some code'
git push origin squash_some_bugs
```
