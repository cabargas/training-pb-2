## What is a Version Control System (VCS)

- Records changes to a file
- Maintains history of changes
- Disaster Recovery
- Types of VCS: Local, Centralized and Distributed
